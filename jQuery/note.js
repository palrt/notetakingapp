$(document).ready(function(){
		var noteList= $("#takingNotes");
		$("#newnotebtn").click(function(){
			console.log("cvbnmdrcfbhnm");
			addNewNote();
		});

		function addNewNote(classes, title, content){
			if(!classes){
				classes = "colors"+ Math.ceil(Math.random()*3);
			}

			noteList.append("<li><div class='"+ classes +"'>" + 
				'<textarea class="note-title" placeholder="Title" maxlength="15"></textarea>'
				+ '<textarea class="note-content" placeholder="Type your content" maxlength="50"></textarea>'
				+ '<img id="close" src="../img/005_055_delete_cross_close_cancel_exit_vote-128.png"/>'
				+'</div></li>');
			
			var newNote = noteList.find("li:last");
			newNote.find("img").click(function() {
				newNote.remove();
				$("#savebtn").click();
			}); 

			newNote.find("textarea.note-title").val(title);
			newNote.find("textarea.note-content").val(content);
			
			newNote.mouseover(function(){
				$(this).find("img").show();
				$(this).find("div").css({"width":"200px", "height":"200px"});
			})

			newNote.mouseleave(function(){
				$(this).find("img").hide();
				$(this).find("div").css({"width":"150px", "height":"150px"});
			})
			
		}

		// loading notes from local storage if available
		var stored = localStorage.getItem("noteList");
			if (stored){
		        var noteArr = JSON.parse(stored);
		        for (var i = 0; i < noteArr.length; i++) {
		        addNewNote(noteArr[i].Class, noteArr[i].Title, noteArr[i].Content);
		     }
		} 
			
		// clicking the 'Save' button to saves notes in localstorage                
		$("#savebtn").click(function() {
			var noteArr = new Array();
			noteList.find("li > div").each(function (i, divVal) {
		    //taking each notes value for title, content and color
		    	var colourClass = $(divVal).attr("class");
		        var title = $(divVal).find("textarea.note-title").val();
		        var content = $(divVal).find("textarea.note-content").val();
		        noteArr.push({ Index: i, Title: title, Content: content, Class: colourClass });
		         
		 	});
		  	var strInJson = JSON.stringify(noteArr);
		      	//Saving notes using localstorge
		        localStorage.setItem("noteList", strInJson);
		    }); 
		     
		});